import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyFirstComponentComponent } from './my-first-component/my-first-component.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';
import { MyOwnComponent } from './my-own/my-own.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SideNavComponent } from './components/side-nav/side-nav.component';
import { TodoComponent } from './components/todo/todo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardItemComponent } from './components/dashboard/dashboard-item/dashboard-item.component';
import { ApiComponent } from './components/api/api.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    SideNavComponent,
    TodoComponent,
    DashboardItemComponent,
    ApiComponent
    // MyFirstComponentComponent,
    // MySecondComponentComponent,
    // MyOwnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    // informacja dla naszej apki, że ma używać ReactiveFroms
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
