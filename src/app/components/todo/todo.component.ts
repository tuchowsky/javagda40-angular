import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { TodoItemInterface } from 'src/app/interfaces/todo-item.interface';
import { SideNavService } from '../services/side-nav.service';
import { TodoServiceService } from '../services/todo-service.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})

export class TodoComponent implements OnInit {
  subscription: Subscription;
  isNavbarOpen: boolean;
  signupForm: FormGroup;
  todoForm: FormGroup;

  todoListData: TodoItemInterface[];
  
  constructor(private sideNavService: SideNavService,
              private todoService: TodoServiceService) { 
      this.subscription = this.sideNavService.isOpen.subscribe(data => {
      this.isNavbarOpen = data;
      console.log(data, 'Todo Component');
    })
  }

  ngOnInit(): void {
    // Signup form
    this.signupForm = new FormGroup({
      'username': new FormControl(null, Validators.required),
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'age': new FormControl(null, [Validators.required, Validators.min(18)])
    });
    // Todo form
    this.todoForm = new FormGroup({
      'description': new FormControl(null, Validators.required)
    });

    this.todoListData = this.todoService.getTodoList();
  }

  onSignupFormSubmit(): void {
    console.log(this.signupForm.get('username').value);
    if(this.signupForm.valid) {
      // do magic
      console.log(this.signupForm.value, 'value from valid form');
      alert('Congrats, your form is valid. More info in Console.');
    }
  }

  onTodoFormSubmit(): void {
    const inputValue = this.todoForm.get('description').value;
    this.todoService.addTodoItem(inputValue);
  }
  
  onDeleteClick(index: number):void {
    this.todoService.deleteTodoItem(index);
  }  

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
