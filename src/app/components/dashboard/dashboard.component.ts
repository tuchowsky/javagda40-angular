import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SideNavService } from '../services/side-nav.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  subscription: Subscription;
  isVisible: boolean = false;
  name: string = 'User';
  namesArray: string[] = [];

  constructor(private sideNavService: SideNavService) {
    // this.isVisible = this.sideNavService.isNavOpen;
    this.subscription = this.sideNavService.isOpen.subscribe(data => {
      this.isVisible = data;
    });
    console.log(this.isVisible, 'jestem z konstruktora');
  }

  ngOnInit(): void { }

  onButtonClick(): void {
    // this.isVisible = !this.isVisible;
    console.log(this.sideNavService.isNavOpen, 'zmienna z servisu');
    this.sideNavService.toggleNavBar();
  }

  onSubmit(): void {
    this.namesArray.push(this.name);
    this.name = '';
    console.log(this.namesArray);
  }

  onDeleteItem(i: number): void {
    this.namesArray.splice(i, 1);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
