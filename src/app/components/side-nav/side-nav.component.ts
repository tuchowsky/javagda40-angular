import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SideNavService } from '../services/side-nav.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit, OnDestroy {
  subscription: Subscription;
  isOpen: boolean = true;

  constructor(private sideNavService: SideNavService) {
    this.subscription = this.sideNavService.isOpen.subscribe(data => {
      this.isOpen = data;
    });
   }

  ngOnInit(): void { }

  onButtonClick(): void {
    this.sideNavService.toggleNavBar();
    // this.isOpen = this.sideNavService.isNavOpen;
    // console.log(this.sideNavService.isNavOpen);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
