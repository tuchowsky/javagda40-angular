import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideNavService {
  // Subjects = Event
  // BehaviorSubject = State
  // BehaviourSubject will return the initial value or the current value on Subscription
  isNavOpen = new BehaviorSubject(false);
  isOpen = this.isNavOpen.asObservable();

  constructor() { }

  toggleNavBar() {
    // this.isNavOpen = !this.isNavOpen;
    this.isNavOpen.next(!this.isNavOpen.value);
    // console.log(this.isNavOpen, 'jestem z servisu');console.log(this.isNavOpen, 'jestem z servisu');
  }
}
