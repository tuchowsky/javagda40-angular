import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SideNavService } from '../services/side-nav.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.scss']
})
export class ApiComponent implements OnInit {
  subscription: Subscription;
  isNavbarOpen: boolean;
  apiData: any = {};
  constructor(private http: HttpClient,
              private sideNavService: SideNavService) {
    this.subscription = this.sideNavService.isOpen.subscribe(isOpen => {
      this.isNavbarOpen = isOpen;
    });
  }

  ngOnInit(): void { }

  onSendRequest() {
    this.http.get('https://reqres.in/api/users?page=1')
    .subscribe(data => {
      console.log(data);
      this.apiData = data;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
