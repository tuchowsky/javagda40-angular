import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApiComponent } from './components/api/api.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TodoComponent } from './components/todo/todo.component';
import { MyFirstComponentComponent } from './my-first-component/my-first-component.component';
import { MySecondComponentComponent } from './my-second-component/my-second-component.component';
// definicja ścieżek na jakich użytkownik ma zobaczyć dany komponent
const routes: Routes = [
  {path: '', component: DashboardComponent},
  {path: 'todo', component: TodoComponent},
  {path: 'api', component: ApiComponent}
  // {path: '', component: MyFirstComponentComponent},
  // {path: 'dashboard', component: MySecondComponentComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
